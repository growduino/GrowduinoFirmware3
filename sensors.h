#pragma once

float getDhtTemperature();

float getDhtHumidity();

void dhtMeasure();

void doPhSetup();
float getPhMeasure();
float calibrate_ph(float raw_data);

void ec_enable();
float getECMeasure();
float calibrate_ec(float pulseTime);

void co2_init();
float getCO2Measure();
float calibrate_co2(float raw_data);
bool getCO2DigitalStatus();


bool th_init();
float get_TH_temperature();
float get_TH_humidity();
