#include "GrowduinoFirmware3.h"

#include <EEPROM.h>

#include <OneWire.h>
#include <DallasTemperature.h>
DallasTemp ds1(ONEWIRE_PIN);
DallasTemp ds2(ONEWIRE_PIN2);

#include <Wire.h>

unsigned int days = 0;

char board_name[10];

String command_line;
String command;
String subcommand;
String value = "";
int startpos;

bool new_thermo = false;

bool just_started = true;

int ups_state = 0;

#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 20, 4);

SensorConfig sensor_config;

Logger Temp1 = Logger("Temp1");
Logger Humidity1 = Logger("Humidity");
Logger Ultrasound = Logger("Usnd");
Logger Light1 = Logger("Light1");
Logger Light2 = Logger("Light2");
Logger Onewire_temp1 = Logger("Temp2");
Logger Onewire_temp2 = Logger("Temp3");
Logger pHmeter = Logger("pH");
Logger ECmeter = Logger("EC");
Logger CO2meter = Logger("CO2");
Logger UPS = Logger("Battery");
Logger PowerState = Logger("PowerState");

Logger * loggers[LOGGERS + 1] = {
  &Temp1,
  &Humidity1,
  &Ultrasound,
  &Light1,
  &Light2,
  &Onewire_temp1,
  &Onewire_temp2,
  &pHmeter,
  &ECmeter,
  &CO2meter,
  &UPS,
  &PowerState
};

int time_offset = 0;

bool ack() {
  Serial.println("Ok");
  return true;
}

bool nack() {
  Serial.println("Not recognized");
  return false;
}

unsigned long getSeconds() {
  unsigned long uptime = millis() / 1000;
  return uptime - time_offset;
}

void setOffset(int offset) {
  time_offset = 0;
  int phys_offset = getSeconds() % 60;
  time_offset = phys_offset - offset;
}

int daymin(unsigned long seconds) {
  return (seconds / 60) % 1440;
}

int daymin() {
  return daymin(getSeconds());
}

bool check_name(char * board_name) {
  // returns true if board has sane name (terminated, human readable string)
  for (int i = 0; i < 10; i++) {
    char ch = board_name[i];
    if (ch == 0x00 ) { // string terminator
      return (i > 0); // return false if board_name is empty; true if not empty.
    };
    if (ch > 'z' || ch < '0') {
      return false; // non-allowed character
    }
  }
  return false; // non terminated string
}

void print_raw(String sensor, float raw_value) {
  Serial.print("{\"sensor\": \"");
  Serial.print(sensor);
  Serial.print("\", \"rawvalue\": \"");
  Serial.print(raw_value);
  Serial.println("\"}");
}


void cmdGet(String subcommand, String value) {
  Serial.println("D: cmdGet");
  if (subcommand.equalsIgnoreCase("name")) {
    Serial.print("{\"name\": \"");
    Serial.print(board_name);
    Serial.println("\"}");

  } else if (subcommand.equalsIgnoreCase("time")) {
    Serial.print("{\"time\": ");
    Serial.print(getSeconds());
    Serial.print(", \"daymin\": ");
    Serial.print(daymin());
    Serial.println("\"}");

  } else if (subcommand.equalsIgnoreCase("sensors")) {
    Serial.print("[");
    for (int i = 0; i < LOGGERS; i++) {
      if (i != 0) {
        Serial.print(",");
      }
      loggers[i]->printjson(&Serial);
    }
    Serial.println("]");

  } else if (subcommand.equalsIgnoreCase("raw")) {
    if (value.equalsIgnoreCase("ph")) {
      print_raw(value, return_middle_f(getPhMeasure));
    } else if (value.equalsIgnoreCase("ec")) {
      print_raw(value, return_middle_f(getECMeasure));
    } else if (value.equalsIgnoreCase("co2")) {
      print_raw(value, return_middle_f(getCO2Measure));
    } else if (value.equalsIgnoreCase("Temp1")) {
      if (new_thermo) {
        print_raw(value, return_middle_f(get_TH_temperature));
      } else {
        dhtMeasure();
        print_raw(value, return_middle_f(getDhtTemperature));
      }
    } else if (value.equalsIgnoreCase("Humidity")) {
      if (new_thermo) {
        print_raw(value, return_middle_f(get_TH_humidity));
      } else {
        dhtMeasure();
        print_raw(value, return_middle_f(getDhtHumidity));
      }
    } else if (value.equalsIgnoreCase("Temp2")) {
      ds1.measure(true);
      print_raw(value, ds1.triple_read());
    } else if (value.equalsIgnoreCase("Temp3")) {
      ds2.measure(true);
      print_raw(value, ds2.triple_read());
    } else if (value.equalsIgnoreCase("Light1")) {
      print_raw(value, perThousand(LIGHT_SENSOR_PIN_1));
    } else if (value.equalsIgnoreCase("Light2")) {
      print_raw(value, perThousand(LIGHT_SENSOR_PIN_2));
    } else if (value.equalsIgnoreCase("Usnd")) {
      print_raw(value, return_middle_f(ultrasound_ping));
    }

  } else if (subcommand.equalsIgnoreCase("config")) {
    Serial.println("{");
    Serial.print("\"co2_400\": \""); Serial.print(sensor_config.co2_400); Serial.println("\",");
    Serial.print("\"co2_40k\": \""); Serial.print(sensor_config.co2_40k); Serial.println("\",");
    Serial.print("\"ph_4\": \""); Serial.print(sensor_config.ph_4); Serial.println("\",");
    Serial.print("\"ph_7\": \""); Serial.print(sensor_config.ph_7); Serial.println("\",");
    Serial.print("\"ec_low_ion\": \""); Serial.print(sensor_config.ec_low_ion); Serial.println("\",");
    Serial.print("\"ec_high_ion\": \""); Serial.print(sensor_config.ec_high_ion); Serial.println("\",");
    Serial.print("\"ec_offset\": \""); Serial.print(sensor_config.ec_offset); Serial.println("\",");
    Serial.print("\"co2_probed\": \""); Serial.print(sensor_config.co2_probed); Serial.println("\",");
    Serial.print("\"co2_digital\": \""); Serial.print(sensor_config.co2_digital); Serial.println("\"");
    Serial.println("}");
  } else {
    nack();
  }
}

bool cmdSet(String subcommand, String value) {
  int valueint = value.toInt();
  char states[OUTPUTS + 1];
  Serial.println("D: cmdSet");

  if (subcommand.equalsIgnoreCase("name")) {

    if (value.length() > 9)
      value = value.substring(0, 9);
    value.toCharArray(board_name, 9);
    board_name[9] = '\0';
    Serial.print("D: setting name to ");
    Serial.println(board_name);
    EEPROM.put(0, board_name);
    return ack();

  } else if (subcommand.equalsIgnoreCase("output") || subcommand.equalsIgnoreCase("outputs")) {
    value.toCharArray(states, OUTPUTS);
    Serial.print("D: setting outputs to ");
    Serial.println(states);
    output_set(states);
    return ack();

  } else if (subcommand.equalsIgnoreCase("offset")) {
    Serial.print("D: Setting offset");
    setOffset(valueint);
    Serial.print(" to ");
    Serial.println(time_offset);
    return ack();
  } else if (subcommand.equalsIgnoreCase("lcd_clear")) {
    Serial.println("D: Clearing lcd");
    lcd_flush();
    lcd_tick();
    return ack();
  } else if (subcommand.equalsIgnoreCase("lcd")) {
    Serial.println("D: Adding line to lcd");
    char * buffer = (char *) "1234567890123456789012";
    value.toCharArray(buffer, LCD_LINE_LEN + 1);
    lcd_publish(buffer);
    return ack();
  }

  if (subcommand.equalsIgnoreCase("co2_400")) {
    if (sensor_config.co2_400 != valueint) {
      sensor_config.co2_400 = valueint;
      EEPROM.put(11, sensor_config);
    }
    return ack();
  }
  if (subcommand.equalsIgnoreCase("co2_40k")) {
    if (sensor_config.co2_40k != valueint) {
      sensor_config.co2_40k = valueint;
      EEPROM.put(11, sensor_config);
    }
    return ack();
  }
  if (subcommand.equalsIgnoreCase("ph_4")) {
    if (sensor_config.ph_4 != valueint) {
      sensor_config.ph_4 = valueint;
      EEPROM.put(11, sensor_config);
    }
    return ack();
  }
  if (subcommand.equalsIgnoreCase("ph_7")) {
    if (sensor_config.ph_7 != valueint) {
      sensor_config.ph_7 = valueint;
      EEPROM.put(11, sensor_config);
    }
    return ack();
  }
  if (subcommand.equalsIgnoreCase("ec_low_ion")) {
    if (sensor_config.ec_low_ion != valueint) {
      sensor_config.ec_low_ion = valueint;
      EEPROM.put(11, sensor_config);
    }
    return ack();
  }
  if (subcommand.equalsIgnoreCase("ec_high_ion")) {
    if (sensor_config.ec_high_ion != valueint) {
      sensor_config.ec_high_ion = valueint;
      EEPROM.put(11, sensor_config);
    }
    return ack();
  }
  if (subcommand.equalsIgnoreCase("ec_offset")) {
    if (sensor_config.ec_offset != valueint) {
      sensor_config.ec_offset = valueint;
      EEPROM.put(11, sensor_config);
    }
    return ack();
  }
  if (subcommand.equalsIgnoreCase("co2_probe")) {
    sensor_config.co2_probed = true;
    sensor_config.co2_digital = getCO2DigitalStatus();
    EEPROM.put(11, sensor_config);
    return ack();
  }
  return nack();
}

void set_default_config() {
  sensor_config.valuecheck = 204;
  sensor_config.co2_400 = 800;
  sensor_config.co2_40k = 600;
  sensor_config.ph_4 = 200;
  sensor_config.ph_7 = 400;
  sensor_config.ec_low_ion = 200;
  sensor_config.ec_high_ion = 100;
  sensor_config.ec_offset = 0;
  sensor_config.co2_probed = false;
  sensor_config.co2_digital = false;
  EEPROM.put(11, sensor_config);
}

void setup() {
  Serial.begin(115200);
  Serial1.begin(9600);
  // put your setup code here, to run once:
  pinMode(13, OUTPUT);
  Wire.begin();
  co2_init();
  EEPROM.get(0, board_name);
  EEPROM.get(11, sensor_config);
  if (!check_name(board_name) || sensor_config.valuecheck != 204) {
    // name or config invalid
    // clear eeprom and set default values
    for (unsigned int i = 0 ; i < EEPROM.length() ; i++) {
      EEPROM.write(i, 0);
    }
    String b_name = "gd3";
    b_name.toCharArray(board_name, 9);
    board_name[9] = '\0';
    EEPROM.put(0, board_name);
    set_default_config();
  }
  Wire.beginTransmission(0x27);
  if (Wire.endTransmission()) {
    lcd.set_address(0x3f);
  }
  Serial.print("Hello ");
  Serial.println(board_name);
  lcd_init();
  lcd_print_immediate(F("starting"));

  ups_init();
  output_init();
  doPhSetup();
  ec_enable();
  //lcd_print_immediate(F("...done"));
}

void loop() {
  float vals[10][3];
  int cycle;
  int idx;

  // put your main code here, to run repeatedly:
  delay(50);
  if (Serial.available()) {
    command_line = "";
    command = "";
    value = "";
    command_line = Serial.readStringUntil('\n');
    Serial.print("D: command_line:");
    Serial.println(command_line);
    command = command_line.substring(0, 3);
    Serial.print("D: command:");
    Serial.println(command);
    command_line = command_line.substring(3);
    command_line.trim();

    startpos = command_line.indexOf(" ");

    if (startpos > 0) {
      subcommand = command_line.substring(0, startpos);
      subcommand.trim();

      value = command_line.substring(startpos);
      value.trim();
    } else {
      subcommand = command_line;
      value = "";
    }

    subcommand.trim();
    Serial.print("D: subcommand:");
    Serial.print(subcommand);
    Serial.println(";");
    if (value != "") {
      Serial.print("D: value:");
      Serial.println(value);
    }

    if (command.equals("set")) {
      cmdSet(subcommand, value);
    } else if (command.equals("get")) {
      cmdGet(subcommand, value);
    }
  }
  lcd_tick();

  if (just_started || (Temp1.Available() && getSeconds() > 30)) {
    if (not sensor_config.co2_probed) {
      sensor_config.co2_probed = true;
      sensor_config.co2_digital = getCO2DigitalStatus();
      EEPROM.put(11, sensor_config);
    }
    just_started = false;
    cycle = 0;
    digitalWrite(13, HIGH);
    new_thermo = th_init();
    doPhSetup();

    for (cycle = 0; cycle < 3; cycle++) {
      idx = 0;
      if (!new_thermo) {
        dhtMeasure();
      }
      ds1.measure(false);
      ds2.measure(true);
      if (!new_thermo) {
        vals[idx++][cycle] = getDhtTemperature();
        vals[idx++][cycle] = getDhtHumidity();
      } else {
        vals[idx++][cycle] = get_TH_temperature();
        vals[idx++][cycle] = get_TH_humidity();
      }
      vals[idx++][cycle] = ultrasound_ping(USOUND_TRG, USOUND_ECHO);
      vals[idx++][cycle] = perThousand(LIGHT_SENSOR_PIN_1);
      vals[idx++][cycle] = perThousand(LIGHT_SENSOR_PIN_2);
      vals[idx++][cycle] = ds1.triple_read();
      vals[idx++][cycle] = ds2.triple_read();
      vals[idx++][cycle] = calibrate_ph(getPhMeasure()) * 100;
      vals[idx++][cycle] = calibrate_ec(getECMeasure());
      vals[idx++][cycle] = calibrate_co2(getCO2Measure());
    }

    idx = 0;
    Temp1.Log(return_middle(vals[idx])); idx++;
    Humidity1.Log(return_middle(vals[idx])); idx++;
    Ultrasound.Log(return_middle(vals[idx])); idx++;
    Light1.Log(return_middle(vals[idx])); idx++;
    Light2.Log(return_middle(vals[idx])); idx++;
    Onewire_temp1.Log(return_middle(vals[idx])); idx++;
    Onewire_temp2.Log(return_middle(vals[idx])); idx++;
    pHmeter.Log(return_middle(vals[idx]) / 100); idx++;
    ECmeter.Log(return_middle(vals[idx])); idx++;
    CO2meter.Log(return_middle(vals[idx])); idx++;

    UPS.Log(ups_read()); // sets ups_state too
    PowerState.Log(ups_state);
    digitalWrite(13, LOW);
  }
}
