#pragma once

void lcd_init();
void lcd_publish(char * msg);
void lcd_publish(const __FlashStringHelper * msg);
// void lcd_publish(const char * text, const char * format, int data);
//void lcd_publish(const char * text, const char * format, int data, float divisor);
void lcd_print_immediate(const __FlashStringHelper * msg);
void lcd_flush();
void lcd_tick();
