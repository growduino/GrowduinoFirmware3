#include "GrowduinoFirmware3.h"

/* ulatrasound distanve meter (water level checking) */

unsigned long timeStart;
unsigned long timeIn;
unsigned long timeOut;
int state;

float timetocm(long mtime) {
  float mtime_f = (float) mtime;
  if (mtime > 0) {
    return mtime_f / 27 / 2 ;
  } else return 0;
}

float ultrasound_ping_inner(int trigger, int echo) {
  pinMode(trigger, OUTPUT);
  pinMode(echo, INPUT);
  state = LOW;

  digitalWrite(trigger, LOW);
  delayMicroseconds(2);
  digitalWrite(trigger, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigger, LOW);

  timeStart = micros();

  while (state == LOW && (micros() - timeStart < 40000)) {
    state = digitalRead(echo);
  }
  timeIn = micros();

  while (state == HIGH && (micros() - timeStart < 40000)) {
    state = digitalRead(echo);
  }
  timeOut = micros();

  if (timeOut - timeStart >= 40000) {
    return MINVALUE;
  }
  return timetocm(timeOut - timeIn);
}

float ultrasound_ping(int trigger, int echo) {
  float distance;
  distance = MINVALUE;
  for (int i = 0; (i < 5 && isinf(distance)) ; i++) {
    delay(10);
    distance = ultrasound_ping_inner(trigger, echo);
  }
  return distance;
}

float ultrasound_ping() {
  return ultrasound_ping(USOUND_TRG, USOUND_ECHO);
}
