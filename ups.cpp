#include "GrowduinoFirmware3.h"
extern int ups_state;

void ups_init() {
  Serial3.begin(19200);
}

float ups_read_inner() {
  char line[64];
  int state;
  int energy;
  energy = -1;
  int counter;

  if (Serial3.available()) {
    strcpy(line, "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0");

    counter = Serial3.readBytesUntil('\n', line, 64);
    line[counter] = '\0';

    if (line[0] != 's') { // UPS stats start with 's'
      return MINVALUE;
    }

    int results = sscanf(line, "s:%i e:%i", &state, &energy);
    if (results < 2 || energy < 0 || energy > 100) {
      return MINVALUE;
    }
    ups_state = state;
    return (float) energy;
  }
  return MINVALUE;

}

float ups_read() {
  while (Serial3.available() > 0 ) {
    Serial3.read();
  }
  delay(100);
  float retval;
  ups_state = 0;
  for (int i = 0; i < 50; i++) {
    retval = ups_read_inner();
    if (retval != MINVALUE) {
      return retval;
    }
    delay(100);
  }
  return MINVALUE;
}
