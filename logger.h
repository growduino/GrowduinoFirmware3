#pragma once


class Logger {
    String name;
    float value;
    unsigned long last_log_time;

  public:

    Logger(String logger_name);
    void printjson(Stream * output);
    bool Available();
    void Log(float new_value);
};

