#include "GrowduinoFirmware3.h"

/* loggers, objects that store measured data */

#include <math.h>


Logger::Logger(String logger_name) {
  name = logger_name;
  last_log_time = 0;
  value = NAN;
}

bool Logger::Available() {
  /*
     returns true if we have not logged this minute
  */

  if (last_log_time == 0) {
    return true;
  }
  return (daymin() != daymin(last_log_time));
}


void Logger::Log(float new_value) {
  last_log_time = getSeconds();
  value = new_value;
}

void Logger::printjson(Stream * output) {
  output->print("{\"name\":\"");
  output->print(name);
  output->print("\",\"value\":");
  if (isnan(value) || isinf(value)) {
    output->print("NaN");
  } else {
    output->print(value);
  }
  output->print(",\"age\":");
  output->print(getSeconds() - last_log_time);
  output->print(",\"idx\":");
  output->print(daymin(last_log_time));

  output->print("}");
}

