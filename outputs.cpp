#include "GrowduinoFirmware3.h"

/* output relay control */

void output_init() {
  for (int i = 0; i < OUTPUTS; i++) {
    pinMode(RELAY_START + i, OUTPUT);
    digitalWrite(RELAY_START + i, LOW);
  }
}

void output_set(char * states) {
  for (int i = 0; i < OUTPUTS; i++) {
    char state = states[i];
    if (state == '\0') {
      break;
    } else if (state == '1') {
      digitalWrite(RELAY_START + i, HIGH);
    } else {
      digitalWrite(RELAY_START + i, LOW);
    }
  }
}

